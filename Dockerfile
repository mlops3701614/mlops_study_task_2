FROM mambaorg/micromamba

COPY environment.yml /app/environment.yml
RUN micromamba env create -f /app/environment.yml

SHELL ["micromamba", "run", "-n", "prod", "/bin/bash", "-c"]

RUN micromamba install -c conda-forge pdm

COPY . /app
WORKDIR /app

USER root
RUN pdm lock
RUN pdm sync
USER mambauser

CMD ["micromamba", "run", "-n", "prod", "python", "test.py"]